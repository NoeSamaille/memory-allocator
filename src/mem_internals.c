/******************************************************
 * Copyright Grégory Mounié 2018                      *
 * This code is distributed under the GLPv3+ licence. *
 * Ce code est distribué sous la licence GPLv3+.      *
 ******************************************************/

#include <sys/mman.h>
#include <assert.h>
#include <stdint.h>
#include "mem.h"
#include "mem_internals.h"

unsigned long knuth_mmix_one_round(unsigned long in)
{
    return in * 6364136223846793005UL % 1442695040888963407UL;
}

void *mark_memarea_and_get_user_ptr(void *ptr, unsigned long size, MemKind k)
{
    // Générer nombre magique
    unsigned long magic = knuth_mmix_one_round((unsigned long) ptr);
    magic = (magic & ~(0b11UL)) | k;
    // Pointeur 64b
    uint64_t * ptr64 = (uint64_t *) ptr;
    // Pointeur 8b
    uint8_t * ptr8 = (uint8_t *) ptr;
    // Taille au debut
    *(ptr64) = size;
    // Nombre magique au debut
    *(ptr64+1) = magic;
    // Nombre magique a la fin
    *(unsigned long *) (ptr8+16+size-32) = magic;
    // Taille a la fin
    *(unsigned long *) (ptr8+24+size-32) = size;
    // Retourne le pointeur sur l espace alloue
    return (ptr64+2);
}

Alloc mark_check_and_get_alloc(void *ptr)
{
    // Pointeur 64b
    uint64_t * ptr64 = (uint64_t *) ptr;
    // Pointeur 8b
    uint8_t * ptr8 = (uint8_t *) ptr;
    // Recupere le nombre magique initial
    unsigned long magic_init = *(unsigned long *)(ptr64-1);
    // Recupere la taille initiale
    unsigned long size_init = *(unsigned long *)(ptr64-2);
    // Recupere le nombre magique final
    unsigned long magic_end = *(unsigned long *)(ptr8+size_init-32);
    // Recupere la taille finale
    unsigned long size_end = *(unsigned long *)(ptr8+size_init-32+8);
    assert(magic_init == magic_end);
    assert(size_init == size_end);
    // Recupere le type d allocation
    MemKind memK = (MemKind) magic_init & 0b11UL;
    // Assure la coherence
    if (size_init-32 >= LARGEALLOC) assert(memK == LARGE_KIND);
    else if (size_init-32 <= SMALLALLOC) assert(memK == SMALL_KIND);
    else assert(memK == MEDIUM_KIND);
    // Retourne l allocation
    Alloc a = {
        (void *) (ptr64-2),
        memK,
        size_init
    };
    return a;
}

unsigned long
mem_realloc_small() {
    assert(arena.chunkpool == 0);
    unsigned long size = (FIRST_ALLOC_SMALL << arena.small_next_exponant);
    arena.chunkpool = mmap(0,
			   size,
			   PROT_READ | PROT_WRITE | PROT_EXEC,
			   MAP_PRIVATE | MAP_ANONYMOUS,
			   -1,
			   0);
    if (arena.chunkpool == MAP_FAILED)
	handle_fatalError("small realloc");
    arena.small_next_exponant++;
    return size;
}

unsigned long
mem_realloc_medium() {
    uint32_t indice = FIRST_ALLOC_MEDIUM_EXPOSANT + arena.medium_next_exponant;
    assert(arena.TZL[indice] == 0);
    unsigned long size = (FIRST_ALLOC_MEDIUM << arena.medium_next_exponant);
    assert( size == (1 << indice));
    arena.TZL[indice] = mmap(0,
			     size*2, // twice the size to allign
			     PROT_READ | PROT_WRITE | PROT_EXEC,
			     MAP_PRIVATE | MAP_ANONYMOUS,
			     -1,
			     0);
    if (arena.TZL[indice] == MAP_FAILED)
	handle_fatalError("medium realloc");
    // align allocation to a multiple of the size
    // for buddy algo
    arena.TZL[indice] += (size - (((intptr_t)arena.TZL[indice]) % size));
    arena.medium_next_exponant++;
    return size; // lie on allocation size, but never free
}


// used for test in buddy algo
unsigned int
nb_TZL_entries() {
    int nb = 0;
    
    for(int i=0; i < TZL_SIZE; i++)
	if ( arena.TZL[i] )
	    nb ++;

    return nb;
}
