/******************************************************
 * Copyright Grégory Mounié 2018                      *
 * This code is distributed under the GLPv3+ licence. *
 * Ce code est distribué sous la licence GPLv3+.      *
 ******************************************************/

#include <assert.h>
#include "mem.h"
#include "mem_internals.h"

void *emalloc_small(unsigned long size)
{
    if (arena.chunkpool == NULL)
    {
        unsigned long size = mem_realloc_small();
        void **cur_addr = (void **)arena.chunkpool;
        for (int i=0; i<(size/CHUNKSIZE)-1; i++)
        {
            *cur_addr = (void *) (cur_addr + (CHUNKSIZE/8));
            cur_addr += (CHUNKSIZE/8);
        }
    }
    void *elt = arena.chunkpool;
    arena.chunkpool += CHUNKSIZE;
    return mark_memarea_and_get_user_ptr(elt, CHUNKSIZE, SMALL_KIND);
}

void efree_small(Alloc a)
{
    *((void **)a.ptr) = NULL;
    void *ptr = arena.chunkpool;
    arena.chunkpool = a.ptr;
    *((void **)arena.chunkpool) = ptr;
}
