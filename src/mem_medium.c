/******************************************************
 * Copyright Grégory Mounié 2018                      *
 * This code is distributed under the GLPv3+ licence. *
 * Ce code est distribué sous la licence GPLv3+.      *
 ******************************************************/

#include <stdint.h>
#include <assert.h>
#include "mem.h"
#include "mem_internals.h"

unsigned int puiss2(unsigned long size)
{
    unsigned int p=0;
    size = size -1; // allocation start in 0
    while(size)
    {  
        // get the largest bit
        p++;
        size >>= 1;
    }
    if (size > (1 << p))
	    p++;
    return p;
}

// Reconstruct tzl for a bloc of size 2^k is needed
void split_worker(unsigned int k)
{
    // Alloue un bloc de taille k
    if (k+1 == FIRST_ALLOC_MEDIUM_EXPOSANT + arena.medium_next_exponant)
    // Besoin de reallouer
    {
        mem_realloc_medium();
    }
    // Regarde le bloc d'indice k+1
    else if (arena.TZL[k+1] == NULL)
    // Le bloc de taille k+1 n'est pas dispo => decoupe recursivement
    {
        split_worker(k+1);
    }
    // Divise en deux le bloc d'indice k+1 en 2
    void *ptr = arena.TZL[k+1];
    arena.TZL[k+1] = *((void **)arena.TZL[k+1]);
    arena.TZL[k] = ptr;
    *((void **)ptr) = (void *)((uintptr_t)ptr^(1<<k));
}

void * emalloc_medium(unsigned long size)
{
    assert(size < LARGEALLOC);
    assert(size > SMALLALLOC);
    unsigned int k = puiss2(size+32);
    if (arena.TZL[k] == NULL)
    // Pas de bloc dispo => split recursivement
    {
        split_worker(k);
    }
    // Perform alloc
    void *ptr = arena.TZL[k];
    arena.TZL[k] = *((void **)arena.TZL[k]);
    return mark_memarea_and_get_user_ptr(ptr, size+32, MEDIUM_KIND);
}

void efree_medium_worker(void *ptr, unsigned long k)
{
    // Calcul le buddy de b
    void *buddy = (void *)((uintptr_t)ptr^(1<<k));
    // Parcours la bonne liste de arena.TZL pour chercher le buddy
    void *cur_ptr = arena.TZL[k];
    void *prev_ptr = cur_ptr;
    while (cur_ptr != NULL && cur_ptr != buddy)
    {
        prev_ptr = cur_ptr;
        cur_ptr = *((void **)cur_ptr);
    }
    if (cur_ptr != NULL)
    // Le buddy est dispo => l'enleve de la list et merge recusrivement
    {
        // Set son suivant = suivant du buddy
        *((void **)prev_ptr) = *((void **)cur_ptr);
        // Suppression du buddy effectuee => recursive merge
        void *next_ptr = (ptr > buddy) ? buddy : ptr;
        efree_medium_worker(next_ptr, k+1);
    }
    else
    // Le buddy est utilise => place le bloc en debut de liste
    {
        *((void **)ptr) = arena.TZL[k];
        arena.TZL[k] = ptr;
    }
}

void efree_medium(Alloc a)
{
    *((void **)a.ptr) = NULL;
    // Calcule l'index k du buddy de a dans arena.TZL
    unsigned int k = puiss2(a.size);
    efree_medium_worker(a.ptr, k);
}
